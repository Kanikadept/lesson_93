const express = require('express');

const Event = require('../models/Event');
const auth = require("../middleware/auth");
const User = require("../models/User");

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        const eventData = {
            name: req.body.name,
            duration: req.body.duration,
            date: req.body.date,
            user: req['user'],
        }

        const event = new Event(eventData);
        const eventResponse = await event.save(event);

        return res.send(eventResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const myEventResponse = await Event.find({user: req['user']});

        // const allowedEvents = [];
        // req['user'].friends.map( async friend => {
        //     const friendFound = User.findOne({_id: friend});
        //
        //     const friendEvents = await Event.find({user: friendFound});
        //     allowedEvents.push(...friendEvents);
        // });
        //
        // allowedEvents.push(...myEventResponse);



        return res.send(myEventResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const eventResponse = await Event.findOne({_id: req.params.id, user: req['user']});
        return res.send(eventResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const eventResponse = await Event.findOneAndDelete({_id: req.params.id, user: req['user']});
        return res.send(eventResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

module.exports = router;