import React from 'react';

import './UserMenu.css';
import {useDispatch} from "react-redux";
import {logoutSuccess} from "../../../store/actions/usersActions";
import {NavLink} from "react-router-dom";

const UserMenu = ({user}) => {

    const dispatch = useDispatch();

    const handleLogout = () => {
        dispatch(logoutSuccess());
    }

    const handleAddEvent = () => {

    }

    return (
        <div className="user-menu">
            <span>Welcome {user.username}!</span>
            <NavLink to="/calendarForm"><button onClick={handleAddEvent}>Add event</button></NavLink>
            <button onClick={handleLogout}>Logout</button>
        </div>
    );
};

export default UserMenu;