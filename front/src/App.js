import {Route, Switch, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";
import Layout from "./Components/Layout/Layout";
import LogIn from "./Containers/Login/LogIn";
import Register from "./Containers/Register/Register";
import './App.css';
import Calendar from "./Containers/Calendar/Calendar";
import CalendarForm from "./Containers/Calendar/CalendarForm/CalendarForm";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo} />;
}

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Switch>
        <Route path="/register" exact component={Register}/>
        <Route path="/" exact component={LogIn}/>
        <ProtectedRoute
          path="/calendar"
          component={Calendar}
          isAllowed={user}
          redirectTo="/"
        />
        <ProtectedRoute
            path="/calendarForm"
            component={CalendarForm}
            isAllowed={user}
            redirectTo="/"
        />
      </Switch>
    </Layout>
  )
}


export default App;
