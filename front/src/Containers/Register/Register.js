import React, {useState} from 'react';
import './Register.css';
import {useDispatch} from "react-redux";
import {registerRequest} from "../../store/actions/usersActions";
import FacebookLogin from "../FacebookLogin/FacebookLogin";

const Register = () => {

    const dispatch = useDispatch();

    const [cred, setCred] = useState({
        username: '',
        password: '',
        email: '',
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setCred(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const handleSubmit = event => {
        event.preventDefault();
        dispatch(registerRequest({...cred}));
    };

    return (
        <form className="register" onSubmit={handleSubmit}>
            <span><i>Sign up</i></span>
            <div className="register__row">
                <label><b>Username:</b></label>
                <input type="text" name="username" value={cred.username} onChange={handleChange}/>
            </div>
            <div className="register__row">
                <label><b>Email:</b></label>
                <input type="email" name="email" value={cred.email} onChange={handleChange}/>
            </div>
            <div className="register__row">
                <label><b>Password:</b></label>
                <input type="password" name="password" value={cred.password} onChange={handleChange}/>
            </div>
            <div className="register__row">
                <button>Sign up</button>
            </div>
            <div className="login__row">
                <FacebookLogin />
            </div>
        </form>
    );
};

export default Register;