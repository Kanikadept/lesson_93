import React from 'react';
import './Event.css';
import {useDispatch} from "react-redux";
import {deleteEventRequest} from "../../../store/actions/eventsActions";

const Event = ({id, name, date, duration}) => {

    const dispatch = useDispatch();


    const handleDeleteEvent = () => {
        dispatch(deleteEventRequest(id));
    }

    return (
        <div className="event">
            <span>Event: {name}</span>
            <span><strong>Date: {date}</strong></span>
            <span>Duration: {duration}</span>
            <button onClick={handleDeleteEvent}>Delete event</button>
        </div>
    );
};

export default Event;