import React, {useState} from 'react';
import './CalendarForm.css';
import {useDispatch} from "react-redux";
import {createEventRequest} from "../../../store/actions/eventsActions";

const CalendarForm = () => {

    const dispatch = useDispatch();

    const [event, setEvent] = useState({
        name: '',
        date: '',
        duration: '',
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setEvent(prevState => ({...prevState, [name]: value}));
    }

    const handleSubmit = e => {
        e.preventDefault();
        dispatch(createEventRequest({...event}));
    }


    return (
        <form className="calendar-form" onSubmit={handleSubmit}>
            <div className="calendar__row">
                <label><b>Name:</b></label>
                <input type="text" name="name" value={event.name} onChange={handleChange}/>
            </div>
            <div className="calendar__row">
                <label><b>Date:</b></label>
                <input type="datetime-local" name="date" value={event.date} onChange={handleChange}/>
            </div>
            <div className="calendar__row">
                <label><b>Duration:</b></label>
                <input type="number" min="1" name="duration" value={event.duration} onChange={handleChange}/>
            </div>
            <div className="calendar__row">
                <button>Add</button>
            </div>
        </form>
    );
};

export default CalendarForm;