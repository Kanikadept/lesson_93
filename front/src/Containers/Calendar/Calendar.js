import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Event from "./Event/Event";
import {fetchEventsRequest} from "../../store/actions/eventsActions";

const Calendar = () => {

    const dispatch = useDispatch();
    const events = useSelector(state => state.events.events);

    useEffect(() => {
        dispatch(fetchEventsRequest());
    }, [dispatch]);

    return (
        <div className="calendar">
            {events.map(event => {
                return <Event key={event._id} id={event._id}
                              name={event.name} duration={event.duration}
                              date={event.date}/>
            })}
        </div>
    );
};

export default Calendar;