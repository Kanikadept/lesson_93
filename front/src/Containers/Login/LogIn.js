import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Alert, AlertTitle} from "@material-ui/lab";
import FacebookLogin from "../FacebookLogin/FacebookLogin";
import './LogIn.css';
import {loginRequest} from "../../store/actions/usersActions";


const LogIn = () => {

    const dispatch = useDispatch();
    const error = useSelector(state => state.users.loginError);

    const [cred, setCred] = useState({
        username: '',
        password: '',
        email: '',
    });

    const handleChange = (event) => {
        const {name, value} = event.target;
        setCred(prev => ({
            ...prev,
            [name]: value
        }));
    }

    const handleSubmit = event => {
        event.preventDefault();
        dispatch(loginRequest({...cred}));
    }


    return (
        <form className="log-in" onSubmit={handleSubmit}>
            <span><i>Log in</i></span>
            {error && (
                <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {error.message || error.global}
                </Alert>
            )}
            <div className="log-in__row">
                <label><b>Username:</b></label>
                <input type="text" name="username" value={cred.username} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Email:</b></label>
                <input type="email" name="email" value={cred.email} onChange={handleChange}/>
            </div>
            <div className="log-in__row">
                <label><b>Password:</b></label>
                <input type="password" name="password" value={cred.password} onChange={handleChange}/>
            </div>
            <div className="login__row">
                <button>Log in</button>
            </div>
            <div className="login__row">
                <FacebookLogin />
            </div>
        </form>
    );
};

export default LogIn;