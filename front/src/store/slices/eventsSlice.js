import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    events: [],
    eventsLoading: false,
    eventsError: false,
    createEventLoading: false,
    createEventError: false,
}

const name = "events";

const eventsSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchEventsRequest: state => {
            state.eventsLoading = true;
        },
        fetchEventsSuccess: (state, {payload: events}) => {
            state.events = events;
            state.eventsLoading = false;
        },
        fetchEventsFailure: (state, payload) => {
            state.eventsError = payload;
            state.eventsLoading = false;
        },
        /////////////////////CREATE EVENT
        createEventRequest: state => {
            state.createEventLoading = true;
        },
        createEventSuccess: state => {
            state.createEventLoading = false;
        },
        createEventFailure: (state, payload) => {
            state.createEventFailure = payload
            state.createEventLoading = false;
        },
        /////////////////////DELETE EVENT
        deleteEventRequest: state => {
            state.deleteEventLoading = true;
        },
        deleteEventSuccess: state => {
            state.deleteEventLoading = false;
        },
        deleteEventFailure: (state, payload) => {
            state.deleteEventFailure = payload
            state.deleteEventLoading = false;
        }
    }
});

export default eventsSlice;