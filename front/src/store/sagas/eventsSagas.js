import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {historyPush} from "../actions/historyActions";
import {NotificationManager} from "react-notifications";
import {createEventSuccess, deleteEventSuccess, fetchEventsSuccess} from "../actions/eventsActions";
import {
    fetchEventsRequest,
    createEventRequest,
    deleteEventRequest,
} from "../actions/eventsActions";


export function* fetchEvents() {
    try {
        const eventsResponse = yield axiosApi.get('/events');
        yield put(fetchEventsSuccess(eventsResponse.data));
    } catch (err) {
        NotificationManager.error('Could not fetch events');
    }
}

export function* createEvent({payload: event}) {
    try {
        const eventsResponse = yield axiosApi.post('/events', event);
        yield put(createEventSuccess(eventsResponse.data));
        yield put(historyPush('/calendar'));
        NotificationManager.success('Event was added!');
    } catch (err) {
        NotificationManager.error('Could not add event');
    }
}

export function* deleteEvent({payload: eventId}) {
    try {
        const eventsResponse = yield axiosApi.delete('/events/'+ eventId);
        yield put(deleteEventSuccess());
        yield put(historyPush('/calendar'));
        yield fetchEvents();
        NotificationManager.success('Event was deleted!');
    } catch (err) {
        NotificationManager.error('Could not delete event');
    }
}
const eventsSagas = [
    takeEvery(fetchEventsRequest, fetchEvents),
    takeEvery(createEventRequest, createEvent),
    takeEvery(deleteEventRequest, deleteEvent)
];

export default eventsSagas;