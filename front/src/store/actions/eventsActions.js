import eventsSlice from "../slices/eventsSlice";

export const {
    fetchEventsSuccess,
    fetchEventsRequest,
    fetchEventsFailure,
    createEventSuccess,
    createEventRequest,
    createEventFailure,
    deleteEventRequest,
    deleteEventSuccess,
    deleteEventFailure,
} = eventsSlice.actions;